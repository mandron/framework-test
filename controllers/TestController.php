<?php

namespace controllers;

use fw\core\base\BaseController;

/**
 * Description of TestController
 *
 * @author mandron
 */
class TestController extends BaseController {
    
    public function actionIndex() {
        return $this->render('test/index', [
            'title' => 'Переданный заголовок',
        ]);
    }
    
    public function actionUpdate() {
        return $this->render('test/update', []);
    }
    
}
