<?php

namespace controllers;

use fw\core\base\BaseController;

/**
 * Description of NewsController
 *
 * @author mandron
 */
class NewsController extends BaseController {
    
    public $layout = 'layouts/news';
    
    public function actionIndex() {
        return $this->render('news/index', []);
    }
    
    public function actionItem() {
        return $this->render('news/item', []);
    }
    
}
