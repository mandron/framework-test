<?php

require_once __DIR__ . '/fw/autoload.php';
$config = require_once __DIR__ . '/config/main.php';

define('__FW_VIEWS_PATH__', __DIR__ . '/views/');

require_once __DIR__ . '/fw/fw.php';

\fw\fw::init($config);

$fw = new fw\core\Application();
$fw->run();
