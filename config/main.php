<?php

return [
    
    'components' => [
        'db' => require_once 'db.php',
        'router' => [
            'class' => 'fw\core\components\router\Router',
            'defaultController' => 'TestController',
        ]
    ]
    
];