<?php

function __autoload($class)
{
    $path = __DIR__ . '/../' . str_replace('\\', '/', $class) . '.php';
    if (file_exists($path)) {
        require_once $path;
    } else {
        throw new Exception('Unable to load class "'.$path.'"');
    }
}