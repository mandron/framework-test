<?php

namespace fw\core;

use fw\core\base\BaseApplication;
use fw\core\config\ConfigChecker;
use fw\core\config\ConfigLoader;

/**
 * class fwBase available everywhere
 *
 * @author mandron
 */
class fwBase {

    /**
     *
     * @var fw\core\
     */
    public static $app;

    public static function init(array $config) {
        self::$app = BaseApplication::getInstance();
        ConfigChecker::run($config);
        ConfigLoader::run($config);
    }

}
