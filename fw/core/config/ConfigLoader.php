<?php

namespace fw\core\config;

use fw\fw;
use fw\core\config\iConfigComponent;

/**
 * Description of ConfigLoader
 *
 * @author mandron
 */
class ConfigLoader implements iConfigComponent {

    public static function run(array $config = []) {
        self::loadComponents();
    }

    /**
     * Loading components from configuration
     */
    private static function loadComponents() {
        foreach (fw::$app->config['components'] as $name => $data) {
            if (self::isComponent($data)) {
                self::addComponent($data['class'], $name);
            }
        }
    }

    /**
     * Checking component data
     * @param array $data
     * @return boolean
     */
    private static function isComponent(array $data) {
        if (isset($data['class']) && class_exists($data['class'])) {
            return true;
        }
        return false;
    }

    /**
     * Adding component to fw
     * @param string $className Component class name
     * @param string $name Component name
     */
    private static function addComponent($className, $name) {
        fw::$app->$name = $className::getInstance();
    }

}
