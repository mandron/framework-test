<?php

namespace fw\core\config;

use fw\fw;
use fw\core\config\iConfigComponent;

/**
 * Description of ConfigChecker
 *
 * @author mandron
 */
class ConfigChecker implements iConfigComponent {

    public static function run(array $config = []) {

        if (empty($config)) {
            throw new Exception('Error with configuraion loading');
        }

        fw::$app->config = $config;
        return true;
    }

}
