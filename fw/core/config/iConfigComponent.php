<?php

namespace fw\core\config;

/**
 * Config component interface
 * @author mandron
 */
interface iConfigComponent {
    
    public static function run(array $config = []);
    
}
