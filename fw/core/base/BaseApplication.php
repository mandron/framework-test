<?php

namespace fw\core\base;

/**
 * Description of BaseApplication
 *
 * @author mandron
 */
class BaseApplication {

    use \fw\core\traits\Singleton;
    
    public $controller;
    
}
