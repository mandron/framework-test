<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace fw\core\base;

/**
 * Description of BaseController
 *
 * @author mandron
 */
class BaseController {
    
    public $action = 'index';
    
    public $layout = 'layouts/default';
    
    public $error = 'default/error';
    
    public function __construct($action = 'index') {
        $this->action = $action;
    }
    
    public function actionError() {
        header("HTTP/1.0 404 Not Found");
        return $this->render($this->error);
    }
    
    public function render($name, $params = []) {
        ob_start();
        ob_implicit_flush(false);
        extract($params, EXTR_OVERWRITE);
        
        include __FW_VIEWS_PATH__ . $name . '.php';
        $content = ob_get_contents();
        ob_clean();
        
        include __FW_VIEWS_PATH__ . $this->layout . '.php';
        
        return ob_get_clean();
    }
    
    public function runAction($action = '') {
        
        if (!empty($action)) {
            $this->action = $action;
        }
        
        $_action_name = 'action' . ucfirst(strtolower($this->action));
        if (is_callable([$this, $_action_name])) {
            return $this->$_action_name();
        } else {
            return $this->actionError();
        }
        
    }
    
}
