<?php

namespace fw\core\components\router;

use fw\fw;
use fw\core\components\Component;

/**
 * Description of Router
 *
 * @author mandron
 */
class Router extends Component {

    use \fw\core\traits\Singleton;
    
    public $defaultController = 'TestController';

    private $_controller_path = 'controllers\\';
    private $_route = [];
    
    /**
     * 
     */
    protected function __construct() {
        
        $this->_parseUrl();
        
        $controller = $this->_controller_path . $this->defaultController;
        if (isset($this->_route['controller'])) {
            $controller = $this->_controller_path . ucfirst($this->_route['controller']) . 'Controller';
        }
        fw::$app->controller = new $controller;
        
        $action = fw::$app->controller->action;
        
        if (isset($this->_route['action'])) {
            $action = $this->_route['action'];
        }
        
        echo fw::$app->controller->runAction($action);
    }
    
    /**
     * 
     * @return type
     */
    private function _parseUrl() {
        $uri = parse_url(filter_input(INPUT_SERVER, 'REQUEST_URI'));
        preg_match_all('/[a-z-0-9]+/', $uri['path'], $matches);
        $url_parts = $matches[0];
        $this->_route = [];

        switch (count($url_parts)) {
            case 2: $this->_route['action'] = strtolower($url_parts[1]);
            case 1: $this->_route['controller'] = strtolower($url_parts[0]);
        }
    }

    public function run() {
        parent::run();
    }

}
