<?php

namespace fw\core\components;

/**
 * Component interface
 * @author mandron
 */
interface iComponent {
    
    public function run();
    
}
