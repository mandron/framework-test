<?php

namespace fw\core;

use fw\fw;

/**
 * Application class
 * Start point of the application
 */
class Application {

    public function __construct(array $config = []) {
        
    }

    public function run() {
        
    }

}
