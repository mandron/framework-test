<?php

namespace fw\core\traits;

/**
 * Trait for singleton classes
 *
 * @author mandron
 */
trait Singleton {
    
    protected function __construct() {}
    protected function __clone() {}
    protected function __wakeup() {}
    
    protected static $_instance = NULL;
    
    public static function getInstance() {
        if (!isset(static::$_instance)) {
            static::$_instance = new static;
        }
        return static::$_instance;
    }
    
}
